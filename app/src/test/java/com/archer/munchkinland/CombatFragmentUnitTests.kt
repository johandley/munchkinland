package com.archer.munchkinland

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.archer.munchkinland.dao.PlayerDao
import com.archer.munchkinland.model.Player
import com.archer.munchkinland.viewmodel.CombatFragmentViewModel
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class CombatFragmentUnitTests {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private val playerDao = mockk<PlayerDao>()

    private val combatFragmentViewModel = CombatFragmentViewModel(playerDao)

    private val players = listOf<Player>(
        Player(0, 0, "New Player", "Male", "Elf", "Cleric", 1, 0),
        Player(1, 0, "New Player", "Male", "Elf", "Cleric", 1, 0),
        Player(2, 0, "New Player", "Male", "Elf", "Cleric", 1, 0)
    )

    @Before
    fun setup() {
        every { playerDao.getPlayerByIdNonLive(0) } returns Player(0, 0, "New Player", "Male", "Elf", "Cleric", 1, 0)
        every { playerDao.getAllPlayersNonLive() } returns players
    }

    @Test
    fun testInitializePlayers() = runBlocking {
        combatFragmentViewModel.initializePlayers(0)
        delay(1)
        assert(combatFragmentViewModel.players.value!!.size == 1)
        assert(combatFragmentViewModel.playerCombinedCombatStrength.value!! == 1)
    }

    @Test
    fun testAddPlayer() = runBlocking {
        combatFragmentViewModel.initializePlayers(0)
        delay(1)
        combatFragmentViewModel.addPlayer(0)
        delay(1)
        assert(combatFragmentViewModel.players.value!!.size == 2)
    }

    @Test
    fun testInitializeMonsters() {
        combatFragmentViewModel.initializeMonsters()
        assert(combatFragmentViewModel.monsters.value!!.size == 1)
        assert(combatFragmentViewModel.monsterCombinedCombatStrength.value!! == 1)
    }

    @Test
    fun testAddMonster() {
        combatFragmentViewModel.initializeMonsters()
        combatFragmentViewModel.addMonster()
        assert(combatFragmentViewModel.monsters.value!!.size == 2)
    }

    @Test
    fun testGetPlayersNotInCombat() = runBlocking {
        val playersNotInCombat = combatFragmentViewModel.getPlayersNotInCombat(0)
        delay(1)
        assert(playersNotInCombat.size == 2)
    }

    @Test
    fun testIncreaseCombinedPlayerCombatStrength() = runBlocking {
        combatFragmentViewModel.initializePlayers(0)
        delay(1)
        combatFragmentViewModel.increaseCombinedPlayerCombatStrength()
        assert(combatFragmentViewModel.playerCombinedCombatStrength.value!! == 2)
    }

    @Test
    fun testDecreaseCombinedPlayerCombatStrength() = runBlocking {
        combatFragmentViewModel.initializePlayers(0)
        delay(1)
        combatFragmentViewModel.decreaseCombinedPlayerCombatStrength()
        assert(combatFragmentViewModel.playerCombinedCombatStrength.value!! == 0)
    }

    @Test
    fun testIncreaseCombinedMonsterCombatStrength() = runBlocking {
        combatFragmentViewModel.initializeMonsters()
        combatFragmentViewModel.increaseCombinedMonsterCombatStrength()
        assert(combatFragmentViewModel.monsterCombinedCombatStrength.value!! == 2)
    }

    @Test
    fun testDecreaseCombinedMonsterCombatStrength() {
        combatFragmentViewModel.initializeMonsters()
        combatFragmentViewModel.decreaseCombinedMonsterCombatStrength()
        assert(combatFragmentViewModel.monsterCombinedCombatStrength.value!! == 0)
    }
}
