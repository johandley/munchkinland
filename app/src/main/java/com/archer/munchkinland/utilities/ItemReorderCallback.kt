package com.archer.munchkinland.utilities

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.archer.munchkinland.interfaces.ItemTouchHelperAdapter

class ItemReorderCallback(private val adapter: ItemTouchHelperAdapter) : ItemTouchHelper.Callback() {
    var oldPosition = -1
    var newPosition = -1

    override fun isLongPressDragEnabled(): Boolean {
        return false
    }

    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int {
        val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN
        return makeMovementFlags(dragFlags, 0)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        newPosition = target.adapterPosition
        adapter.onItemMove(viewHolder.adapterPosition, target.adapterPosition)
        return true
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        super.onSelectedChanged(viewHolder, actionState)
        when (actionState) {
            ItemTouchHelper.ACTION_STATE_DRAG -> {
                viewHolder?.adapterPosition?.let { oldPosition = it }
            }
            ItemTouchHelper.ACTION_STATE_IDLE -> {
                adapter.swapPositionRecords(oldPosition, newPosition)
                oldPosition = -1
                newPosition = -1
            }
        }
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        TODO("Not yet implemented")
    }
}
