package com.archer.munchkinland.utilities

import android.app.AlertDialog
import android.content.Context
import android.content.res.Resources
import android.widget.*
import com.archer.munchkinland.R
import com.archer.munchkinland.enums.ClassEnum
import com.archer.munchkinland.enums.RaceEnum
import com.archer.munchkinland.enums.SexEnum
import com.archer.munchkinland.model.Player
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class AddPlayerAlertDialogUtilities {

    companion object {

        // initializations
        fun initializeAutoCompleteTextViews(dialog: AlertDialog, resources: Resources, context: Context) {
            val race1TextView = dialog.findViewById<AutoCompleteTextView>(R.id.race_1_text_view)
            val race1TextInputLayout = dialog.findViewById<TextInputLayout>(R.id.race_1_text_input_layout)
            val race2TextView = dialog.findViewById<AutoCompleteTextView>(R.id.race_2_text_view)
            val race2TextInputLayout = dialog.findViewById<TextInputLayout>(R.id.race_2_text_input_layout)

            val class1TextView = dialog.findViewById<AutoCompleteTextView>(R.id.class_1_text_view)
            val class1TextInputLayout = dialog.findViewById<TextInputLayout>(R.id.class_1_text_input_layout)
            val class2TextView = dialog.findViewById<AutoCompleteTextView>(R.id.class_2_text_view)
            val class2TextInputLayout = dialog.findViewById<TextInputLayout>(R.id.class_2_text_input_layout)

            setAutoCompleteTextViewArrayAdapter(race1TextView, resources.getStringArray(R.array.race_array), context)
            setAutoCompleteTextViewArrayAdapter(race2TextView, resources.getStringArray(R.array.race_plus_none_array), context)
            setAutoCompleteTextViewArrayAdapter(class1TextView, resources.getStringArray(R.array.class_array), context)
            setAutoCompleteTextViewArrayAdapter(class2TextView, resources.getStringArray(R.array.class_array), context)

            setAutoCompleteTextViewOnItemClickListener(race1TextView, race1TextInputLayout)
            setAutoCompleteTextViewOnItemClickListener(race2TextView, race2TextInputLayout)
            setAutoCompleteTextViewOnItemClickListener(class1TextView, class1TextInputLayout)
            setAutoCompleteTextViewOnItemClickListener(class2TextView, class2TextInputLayout)
        }

        fun initializeAddPlayerDialogButtons(builder: AlertDialog.Builder, context: Context) {
            builder.setNegativeButton("Cancel") { _, _ ->
                Toast.makeText(context, "Munchkin not created", Toast.LENGTH_SHORT).show()
            }
            builder.setPositiveButton("Add Munchkin") { _, _ ->
            }
        }

        fun initializeEditPlayerDialogButtons(builder: AlertDialog.Builder, context: Context) {
            builder.setNegativeButton("Cancel") { _, _ ->
                Toast.makeText(context, "Munchkin not updated", Toast.LENGTH_SHORT).show()
            }
            builder.setPositiveButton("Update Munchkin") { _, _ ->
            }
        }

        fun initializeDefaultValues(dialog: AlertDialog, player: Player) {
            initializeName(dialog, player)
            initializeSex(dialog, player)
            initializeRaces(dialog, player)
            initializeClasses(dialog, player)
        }

        private fun initializeName(dialog: AlertDialog, player: Player) {
            val playerNameEditText = dialog.findViewById<EditText>(R.id.player_name_edit_text)
            playerNameEditText.setText(player.name)
        }

        private fun initializeSex(dialog: AlertDialog, player: Player) {
            val maleRadioButton = dialog.findViewById<RadioButton>(R.id.male_radio_button)
            val femaleRadioButton = dialog.findViewById<RadioButton>(R.id.female_radio_button)
            if (player.sex == "Male") {
                maleRadioButton.isChecked = true
            } else if (player.sex == "Female") {
                femaleRadioButton.isChecked = true
            }
        }

        private fun initializeRaces(dialog: AlertDialog, player: Player) {
            val race1TextView = dialog.findViewById<AutoCompleteTextView>(R.id.race_1_text_view)
            val race2TextView = dialog.findViewById<AutoCompleteTextView>(R.id.race_2_text_view)
            val race = player.race
            val racesSplit = race.split("-")
            if (racesSplit.size == 1) {
                race1TextView.setText(racesSplit[0])
            } else if (racesSplit.size == 2) {
                race1TextView.setText(racesSplit[0])
                race2TextView.setText(racesSplit[1])
            }
        }

        private fun initializeClasses(dialog: AlertDialog, player: Player) {
            val class1TextView = dialog.findViewById<AutoCompleteTextView>(R.id.class_1_text_view)
            val class2TextView = dialog.findViewById<AutoCompleteTextView>(R.id.class_2_text_view)
            val clazz = player.clazz
            val classesSplit = clazz.split("-")
            if (classesSplit.size == 1) {
                class1TextView.setText(classesSplit[0])
            } else if (classesSplit.size == 2) {
                class1TextView.setText(classesSplit[0])
                class2TextView.setText(classesSplit[1])
            }
        }

        private fun setAutoCompleteTextViewOnItemClickListener(view: AutoCompleteTextView, textInputLayout: TextInputLayout) {
            view.setOnItemClickListener { _, _, _, _ ->
                textInputLayout.isErrorEnabled = false
            }
        }

        private fun setAutoCompleteTextViewArrayAdapter(view: AutoCompleteTextView, strings: Array<String>, context: Context) {
            ArrayAdapter(context, R.layout.auto_complete_text_view_dropdown_item, strings)
                .also {
                    view.setAdapter(it)
                }
        }

        // getters
        fun getName(dialog: AlertDialog): String {
            val nameEditText = dialog.findViewById<TextInputEditText>(R.id.player_name_edit_text)
            return nameEditText?.text.toString()
        }

        fun getSex(dialog: AlertDialog): String {
            val maleRadioButton = dialog.findViewById<RadioButton>(R.id.male_radio_button)
            return if (maleRadioButton?.isChecked == true) {
                SexEnum.toLongName(SexEnum.MALE)
            } else {
                SexEnum.toLongName(SexEnum.FEMALE)
            }
        }

        fun getRace(dialog: AlertDialog): String {
            val race1TextView = dialog.findViewById<AutoCompleteTextView>(R.id.race_1_text_view)
            val race2TextView = dialog.findViewById<AutoCompleteTextView>(R.id.race_2_text_view)
            val race1Text = race1TextView.text.toString()
            val race2Text = race2TextView.text.toString()
            val race1 = if (race1Text.isBlank()) "" else RaceEnum.toLongName(RaceEnum.fromLongName(race1Text))
            val race2 = if (race2Text.isBlank() || race2Text == "None") "" else RaceEnum.toLongName(RaceEnum.fromLongName(race2Text))
            return concatenateRaces(race1, race2)
        }

        fun getClass(dialog: AlertDialog): String {
            val clazz1TextView = dialog.findViewById<AutoCompleteTextView>(R.id.class_1_text_view)
            val clazz2TextView = dialog.findViewById<AutoCompleteTextView>(R.id.class_2_text_view)
            val clazz1Text = clazz1TextView.text.toString()
            val clazz2Text = clazz2TextView.text.toString()
            val clazz1 = if (clazz1Text.isBlank()) "None" else ClassEnum.toLongName(ClassEnum.fromLongName(clazz1Text))
            val clazz2 = if (clazz2Text.isBlank()) "None" else ClassEnum.toLongName(ClassEnum.fromLongName(clazz2Text))
            return concatenateClasses(clazz1, clazz2)
        }

        // getter utilities
        private fun concatenateRaces(race1: String, race2: String): String {
            return if (race1.isBlank()) {
                race2
            } else if (race2.isBlank()) {
                race1
            } else if (race1.isBlank() && race2.isBlank()) {
                "Human"
            } else {
                "$race1-$race2"
            }
        }

        private fun concatenateClasses(clazz1: String, clazz2: String): String {
            return if (clazz1 == "None" && clazz2 == "None") {
                ""
            } else if (clazz1 == "None") {
                clazz2
            } else if (clazz2 == "None") {
                clazz1
            } else {
                "$clazz1-$clazz2"
            }
        }

        // validation
        fun hasErrors(dialog: AlertDialog): Boolean {
            var hasErrors = false
            val playerNameEditText = dialog.findViewById<EditText>(R.id.player_name_edit_text)
            val race1TextView = dialog.findViewById<AutoCompleteTextView>(R.id.race_1_text_view)
            val race1TextInputLayout = dialog.findViewById<TextInputLayout>(R.id.race_1_text_input_layout)
            val race2TextView = dialog.findViewById<AutoCompleteTextView>(R.id.race_2_text_view)
            val race2TextInputLayout = dialog.findViewById<TextInputLayout>(R.id.race_2_text_input_layout)
            val class1TextView = dialog.findViewById<AutoCompleteTextView>(R.id.class_1_text_view)
            val class2TextView = dialog.findViewById<AutoCompleteTextView>(R.id.class_2_text_view)
            val class2TextInputLayout = dialog.findViewById<TextInputLayout>(R.id.class_2_text_input_layout)

            hasErrors = if (validateNameSet(playerNameEditText)) true else hasErrors
            hasErrors = if (validateRaceSet(race1TextView, race1TextInputLayout)) true else hasErrors
            hasErrors = if (validateRaces(race1TextView, race2TextView, race2TextInputLayout)) true else hasErrors
            hasErrors = if (validateClasses(class1TextView, class2TextView, class2TextInputLayout)) true else hasErrors
            return hasErrors
        }

        private fun validateNameSet(playerNameEditText: EditText): Boolean {
            return if (playerNameEditText.text.isBlank()) {
                playerNameEditText.error = "Name must not be blank"
                true
            } else {
                false
            }
        }

        private fun validateRaceSet(race1TextView: AutoCompleteTextView, race1TextInputLayout: TextInputLayout): Boolean {
            return if (race1TextView.text.isBlank()) {
                race1TextInputLayout.error = "Race 1 must not be blank"
                true
            } else {
                false
            }
        }

        private fun validateRaces(race1TextView: AutoCompleteTextView, race2TextView: AutoCompleteTextView, race2TextInputLayout: TextInputLayout): Boolean {
            val race1 = race1TextView.text.toString()
            val race2 = race2TextView.text.toString()
            return if (race1 == race2) {
                race2TextInputLayout.error = "Races Must Be Different"
                true
            } else {
                false
            }
        }

        private fun validateClasses(class1TextView: AutoCompleteTextView, class2TextView: AutoCompleteTextView, class2TextInputLayout: TextInputLayout): Boolean {
            val class1 = class1TextView.text.toString()
            val class2 = class2TextView.text.toString()
            return if ((class1 != "" || class2 != "") && class1 == class2) {
                class2TextInputLayout.error = "Classes Must Be Different"
                true
            } else {
                false
            }
        }
    }
}
