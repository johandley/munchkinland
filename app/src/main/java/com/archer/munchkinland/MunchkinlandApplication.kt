package com.archer.munchkinland

import android.app.Application
import com.archer.munchkinland.dependencyinjection.databaseModule
import com.archer.munchkinland.dependencyinjection.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext.startKoin
import org.koin.core.logger.Level

class MunchkinlandApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(if (BuildConfig.DEBUG) Level.ERROR else Level.NONE)
            androidContext(this@MunchkinlandApplication)
            modules(listOf(databaseModule, viewModelModule))
        }
    }
}
