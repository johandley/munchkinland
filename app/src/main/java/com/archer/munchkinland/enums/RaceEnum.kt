package com.archer.munchkinland.enums

enum class RaceEnum {
    ELF, DWARF, HALFLING, ORC, GNOME, HUMAN;

    companion object {
        fun fromLongName(longName: String): RaceEnum {
            return when (longName) {
                "Elf" -> ELF
                "Dwarf" -> DWARF
                "Halfling" -> HALFLING
                "Orc" -> ORC
                "Gnome" -> GNOME
                "Human" -> HUMAN
                else -> throw IllegalArgumentException()
            }
        }

        fun toLongName(raceEnum: RaceEnum): String {
            return when (raceEnum) {
                ELF -> "Elf"
                DWARF -> "Dwarf"
                HALFLING -> "Halfling"
                ORC -> "Orc"
                GNOME -> "Gnome"
                HUMAN -> "Human"
            }
        }
    }
}
