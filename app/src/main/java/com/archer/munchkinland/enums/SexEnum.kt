package com.archer.munchkinland.enums

enum class SexEnum {
    MALE, FEMALE;

    companion object {
        fun fromLongName(longName: String): SexEnum {
            return when (longName) {
                "Male" -> MALE
                "Female" -> FEMALE
                else -> throw IllegalArgumentException()
            }
        }

        fun toLongName(sexEnum: SexEnum): String {
            return when (sexEnum) {
                MALE -> "Male"
                FEMALE -> "Female"
            }
        }
    }
}
