package com.archer.munchkinland.enums

enum class ClassEnum {
    WIZARD, WARRIOR, THIEF, CLERIC, BARD, RANGER, NONE;

    companion object {
        fun fromLongName(longName: String): ClassEnum {
            return when (longName) {
                "Wizard" -> WIZARD
                "Warrior" -> WARRIOR
                "Thief" -> THIEF
                "Cleric" -> CLERIC
                "Bard" -> BARD
                "Ranger" -> RANGER
                "None" -> NONE
                else -> throw IllegalArgumentException()
            }
        }

        fun toLongName(classEnum: ClassEnum): String {
            return when (classEnum) {
                WIZARD -> "Wizard"
                WARRIOR -> "Warrior"
                THIEF -> "Thief"
                CLERIC -> "Cleric"
                BARD -> "Bard"
                RANGER -> "Ranger"
                NONE -> "None"
            }
        }
    }
}
