package com.archer.munchkinland.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.archer.munchkinland.dao.PlayerDao
import com.archer.munchkinland.dao.PositionRecordDao
import com.archer.munchkinland.model.Player
import com.archer.munchkinland.model.PositionRecord

@Database(entities = [Player::class, PositionRecord::class], version = 1)
abstract class PlayerDatabase : RoomDatabase() {
    abstract fun playerDao(): PlayerDao
    abstract fun positionRecordDao(): PositionRecordDao
}
