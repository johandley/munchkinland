package com.archer.munchkinland.interfaces

interface ItemTouchHelperAdapter {
    fun onItemMove(fromPosition: Int, toPosition: Int): Boolean
    fun swapPositionRecords(fromPosition: Int, toPosition: Int)
    fun onItemDismiss(position: Int)
}
