package com.archer.munchkinland.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.archer.munchkinland.model.Player

@Dao
interface PlayerDao {
    @Query("SELECT * FROM player")
    fun getAllPlayers(): LiveData<List<Player>>

    @Query("SELECT * FROM player")
    fun getAllPlayersNonLive(): List<Player>

    @Query("SELECT * FROM player ORDER BY positionId ASC")
    fun getAllPlayersNonLiveOrderByPositionId(): List<Player>

    @Query("SELECT * FROM player ORDER BY positionId ASC")
    fun getAllOrderByPositionId(): LiveData<List<Player>>

    @Query("SELECT * FROM player WHERE id = :id")
    fun getPlayerById(id: Long): LiveData<Player>

    @Query("SELECT * FROM player WHERE id = :id")
    fun getPlayerByIdNonLive(id: Long): Player

    @Insert
    fun insertPlayer(player: Player)

    @Update
    fun updatePlayer(newPlayer: Player)

    @Delete
    fun deletePlayer(player: Player)
}
