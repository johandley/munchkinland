package com.archer.munchkinland.dao

import androidx.room.Dao
import androidx.room.Insert
import com.archer.munchkinland.model.PositionRecord

@Dao
interface PositionRecordDao {
    @Insert
    fun insertPositionRecord(positionRecord: PositionRecord): Long
}
