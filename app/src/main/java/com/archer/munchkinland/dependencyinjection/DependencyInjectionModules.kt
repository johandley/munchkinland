package com.archer.munchkinland.dependencyinjection

import android.app.Application
import androidx.room.Room
import com.archer.munchkinland.dao.PlayerDao
import com.archer.munchkinland.dao.PositionRecordDao
import com.archer.munchkinland.database.PlayerDatabase
import com.archer.munchkinland.viewmodel.CombatFragmentViewModel
import com.archer.munchkinland.viewmodel.PlayerFragmentViewModel
import com.archer.munchkinland.viewmodel.PlayerListFragmentViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val databaseModule = module {
    fun provideDataBase(application: Application): PlayerDatabase {
        return Room.databaseBuilder(application, PlayerDatabase::class.java, "PLAYERDB")
            .fallbackToDestructiveMigration()
            .build()
    }

    fun providePlayerDao(dataBase: PlayerDatabase): PlayerDao {
        return dataBase.playerDao()
    }

    fun providePositionRecordDao(database: PlayerDatabase): PositionRecordDao {
        return database.positionRecordDao()
    }

    single { provideDataBase(androidApplication()) }
    single { providePlayerDao(get()) }
    single { providePositionRecordDao(get()) }
}

val viewModelModule = module {
    viewModel { PlayerListFragmentViewModel(get(), get()) }
    viewModel { PlayerFragmentViewModel(get()) }
    viewModel { CombatFragmentViewModel(get()) }
}
