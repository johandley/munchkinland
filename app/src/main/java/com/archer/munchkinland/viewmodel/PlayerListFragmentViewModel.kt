package com.archer.munchkinland.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.archer.munchkinland.dao.PlayerDao
import com.archer.munchkinland.dao.PositionRecordDao
import com.archer.munchkinland.model.Player
import com.archer.munchkinland.model.PositionRecord

class PlayerListFragmentViewModel(
    private val playerDao: PlayerDao,
    private val positionRecordDao: PositionRecordDao
) : ViewModel() {

    fun getAllPlayersOrderByPositionId(): LiveData<List<Player>> {
        return playerDao.getAllOrderByPositionId()
    }

    fun getAllPlayersNonLiveOrderByPositionId(): List<Player> {
        return playerDao.getAllPlayersNonLiveOrderByPositionId()
    }

    private fun getPlayerById(id: Long): Player {
        return playerDao.getPlayerByIdNonLive(id)
    }

    fun addPlayer(player: Player) = playerDao.insertPlayer(player)

    fun updatePlayer(newPlayer: Player) = playerDao.updatePlayer(newPlayer)

    fun swapPlayerPositionRecords(fromPosition: Int, toPosition: Int) {
        if (fromPosition < 0 || toPosition < 0 || fromPosition == toPosition) return
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                val players = getAllPlayersNonLiveOrderByPositionId()
                val playerOneId = players[i].id
                val playerTwoId = players[i + 1].id
                val playerOne = getPlayerById(playerOneId)
                val playerTwo = getPlayerById(playerTwoId)
                val temp = playerOne.copy()
                playerOne.positionId = playerTwo.positionId
                playerTwo.positionId = temp.positionId
                playerDao.updatePlayer(playerOne)
                playerDao.updatePlayer(playerTwo)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                val players = getAllPlayersNonLiveOrderByPositionId()
                val playerOneId = players[i].id
                val playerTwoId = players[i - 1].id
                val playerOne = getPlayerById(playerOneId)
                val playerTwo = getPlayerById(playerTwoId)
                val temp = playerOne.copy()
                playerOne.positionId = playerTwo.positionId
                playerTwo.positionId = temp.positionId
                playerDao.updatePlayer(playerOne)
                playerDao.updatePlayer(playerTwo)
            }
        }
    }

    fun deletePlayer(id: Long) {
        val player = getPlayerById(id)
        playerDao.deletePlayer(player)
    }

    fun createPositionRecord(positionRecord: PositionRecord): Long {
        return positionRecordDao.insertPositionRecord(positionRecord)
    }
}
