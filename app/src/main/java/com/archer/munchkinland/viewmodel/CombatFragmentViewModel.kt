package com.archer.munchkinland.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.archer.munchkinland.dao.PlayerDao
import com.archer.munchkinland.model.Monster
import com.archer.munchkinland.model.Player
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CombatFragmentViewModel(private val playerDao: PlayerDao) : ViewModel() {

    val playerCombinedCombatStrength: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>()
    }
    val monsterCombinedCombatStrength: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>()
    }
    val players: MutableLiveData<List<Player>> by lazy {
        MutableLiveData<List<Player>>()
    }

    val monsters: MutableLiveData<List<Monster>> by lazy {
        MutableLiveData<List<Monster>>()
    }

    fun initializePlayers(id: Long) {
        CoroutineScope(Dispatchers.IO).launch {
            val newPlayers = listOf(getPlayerById(id))
            players.postValue(newPlayers)
            initializePlayerCombatStrength(newPlayers)
        }
    }

    fun initializeMonsters() {
        monsters.postValue(listOf(Monster(1, 0)))
        initializeMonsterCombatStrength()
    }

    private fun initializePlayerCombatStrength(startingPlayers: List<Player>) {
        var combatStrength = 0
        startingPlayers.forEach {
            combatStrength += it.level
            combatStrength += it.gear
        }
        playerCombinedCombatStrength.postValue(combatStrength)
    }

    private fun initializeMonsterCombatStrength() {
        monsterCombinedCombatStrength.postValue(1)
    }

    fun addPlayer(id: Long) {
        CoroutineScope(Dispatchers.IO).launch {
            val currPlayers = players.value
            val newPlayer = getPlayerById(id)
            val players = currPlayers!!.plus(newPlayer)
            this@CombatFragmentViewModel.players.postValue(players)
            var combatStrength = playerCombinedCombatStrength.value!!
            combatStrength += newPlayer.level
            combatStrength += newPlayer.gear
            this@CombatFragmentViewModel.playerCombinedCombatStrength.postValue(combatStrength)
        }
    }

    fun addMonster() {
        val newMonsters = monsters.value!!.plus(Monster(1, 0))
        monsters.postValue(newMonsters)
        var combatStrength = monsterCombinedCombatStrength.value!!
        combatStrength++
        monsterCombinedCombatStrength.postValue(combatStrength)
    }

    private fun updatePlayer(player: Player) {
        CoroutineScope(Dispatchers.IO).launch {
            playerDao.updatePlayer(player)
            refreshPlayers()
        }
    }

    fun getPlayersNotInCombat(combatInitiatorId: Long): List<Player> {
        val playerInCombat = getPlayerById(combatInitiatorId)
        val allPlayers = playerDao.getAllPlayersNonLive()
        val playersNotInCombat = ArrayList<Player>()
        allPlayers.forEach {
            if (it.id != playerInCombat.id) {
                playersNotInCombat.add(it)
            }
        }
        return playersNotInCombat
    }

    private fun getPlayerById(id: Long): Player {
        return playerDao.getPlayerByIdNonLive(id)
    }

    private fun refreshPlayers() {
        val newPlayers = ArrayList<Player>()
        players.value?.forEach {
            val currentPlayer = getPlayerById(it.id)
            newPlayers.add(currentPlayer)
        }
        players.postValue(newPlayers)
    }

    fun increaseCombinedPlayerCombatStrength() {
        var currentCombinedCombatStrength = playerCombinedCombatStrength.value
        if (currentCombinedCombatStrength != null) {
            currentCombinedCombatStrength += 1
        }
        playerCombinedCombatStrength.postValue(currentCombinedCombatStrength)
    }

    fun decreaseCombinedPlayerCombatStrength() {
        var currentCombinedCombatStrength = playerCombinedCombatStrength.value
        if (currentCombinedCombatStrength != null) {
            currentCombinedCombatStrength -= 1
        }
        playerCombinedCombatStrength.postValue(currentCombinedCombatStrength)
    }

    fun increaseCombinedMonsterCombatStrength() {
        var currentCombinedCombatStrength = monsterCombinedCombatStrength.value
        if (currentCombinedCombatStrength != null) {
            currentCombinedCombatStrength += 1
        }
        monsterCombinedCombatStrength.postValue(currentCombinedCombatStrength)
    }

    fun decreaseCombinedMonsterCombatStrength() {
        var currentCombinedCombatStrength = monsterCombinedCombatStrength.value
        if (currentCombinedCombatStrength != null) {
            currentCombinedCombatStrength -= 1
        }
        monsterCombinedCombatStrength.postValue(currentCombinedCombatStrength)
    }

    fun increasePlayerLevel(player: Player) {
        player.level += 1
        updatePlayer(player)
    }

    fun decreasePlayerLevel(player: Player) {
        player.level -= 1
        updatePlayer(player)
    }

    fun increasePlayerGear(player: Player) {
        player.gear += 1
        updatePlayer(player)
    }

    fun decreasePlayerGear(player: Player) {
        player.gear -= 1
        updatePlayer(player)
    }

    fun removePlayerFromCombat(player: Player, modifier: Int) {
        val newPlayers = ArrayList<Player>()
        players.value!!.forEach {
            if (it.id != player.id) {
                newPlayers.add(it)
            } else {
                var newCombatStrength = playerCombinedCombatStrength.value!!
                newCombatStrength -= it.gear + it.level + modifier
                playerCombinedCombatStrength.postValue(newCombatStrength)
            }
        }
        players.postValue(newPlayers)
    }

    fun increaseMonsterLevel(position: Int) {
        val currentMonsters = monsters.value
        if (currentMonsters != null) {
            val newMonster = currentMonsters[position]
            newMonster.level = newMonster.level + 1
            var iteratorPosition = 0
            val newMonsters = ArrayList<Monster>()
            currentMonsters.forEach {
                if (iteratorPosition == position) {
                    newMonsters.add(newMonster)
                } else {
                    newMonsters.add(it)
                }
                iteratorPosition += 1
            }
            monsters.postValue(newMonsters)
        }
    }

    fun decreaseMonsterLevel(position: Int) {
        val currentMonsters = monsters.value
        if (currentMonsters != null) {
            val newMonster = currentMonsters[position]
            newMonster.level = newMonster.level - 1
            var iteratorPosition = 0
            val newMonsters = ArrayList<Monster>()
            currentMonsters.forEach {
                if (iteratorPosition == position) {
                    newMonsters.add(newMonster)
                } else {
                    newMonsters.add(it)
                }
                iteratorPosition += 1
            }
            monsters.postValue(newMonsters)
        }
    }

    fun increaseMonsterModifier(position: Int) {
        val currentMonsters = monsters.value
        if (currentMonsters != null) {
            val newMonster = currentMonsters[position]
            newMonster.modifier = newMonster.modifier + 1
            var iteratorPosition = 0
            val newMonsters = ArrayList<Monster>()
            currentMonsters.forEach {
                if (iteratorPosition == position) {
                    newMonsters.add(newMonster)
                } else {
                    newMonsters.add(it)
                }
                iteratorPosition += 1
            }
            monsters.postValue(newMonsters)
        }
    }

    fun decreaseMonsterModifier(position: Int) {
        val currentMonsters = monsters.value
        if (currentMonsters != null) {
            val newMonster = currentMonsters[position]
            newMonster.modifier = newMonster.modifier - 1
            var iteratorPosition = 0
            val newMonsters = ArrayList<Monster>()
            currentMonsters.forEach {
                if (iteratorPosition == position) {
                    newMonsters.add(newMonster)
                } else {
                    newMonsters.add(it)
                }
                iteratorPosition += 1
            }
            monsters.postValue(newMonsters)
        }
    }

    fun removeMonsterFromCombat(position: Int) {
        val newMonsters = ArrayList<Monster>()
        var i = 0
        monsters.value!!.forEach {
            if (i != position) {
                newMonsters.add(it)
            } else {
                var newCombatStrength = monsterCombinedCombatStrength.value!!
                newCombatStrength -= it.level + it.modifier
                monsterCombinedCombatStrength.postValue(newCombatStrength)
            }
            i++
        }
        monsters.postValue(newMonsters)
    }

    fun resolveCombat() {
        val playersInCombat = players.value
        if (playersWon()) {
            resolveCombatVictory(playersInCombat)
        }
    }

    private fun resolveCombatVictory(playersInCombat: List<Player>?) {
        if (playersInCombat == null) return
        if (playersInCombat.size == 1) {
            increasePlayerLevel(playersInCombat[0])
        } else {
            if (playersInCombat[1].race.contains("Elf")) {
                increasePlayerLevel(playersInCombat[0])
                increasePlayerLevel(playersInCombat[1])
            }
        }
    }

    private fun playersWon(): Boolean {
        val playersCombatStrength = playerCombinedCombatStrength.value
        val monstersCombatStrength = monsterCombinedCombatStrength.value
        return if (playersCombatStrength != null && monstersCombatStrength != null) {
            playersCombatStrength > monstersCombatStrength
        } else {
            false
        }
    }
}
