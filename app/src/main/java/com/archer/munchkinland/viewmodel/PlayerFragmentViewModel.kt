package com.archer.munchkinland.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.archer.munchkinland.dao.PlayerDao
import com.archer.munchkinland.model.Player
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PlayerFragmentViewModel(private val playerDao: PlayerDao) : ViewModel() {
    lateinit var player: LiveData<Player>

    fun initializePlayer(id: Long) {
        player = playerDao.getPlayerById(id)
    }

    fun getAllPlayersOrderByPositionId(): LiveData<List<Player>> {
        return playerDao.getAllOrderByPositionId()
    }

    fun updatePlayer(newPlayer: Player) {
        CoroutineScope(Dispatchers.IO).launch {
            playerDao.updatePlayer(newPlayer)
        }
    }

    fun killPlayer(player: Player) {
        player.gear = 0
        updatePlayer(player)
    }

    fun resetPlayer(player: Player) {
        player.gear = 0
        player.level = 1
        updatePlayer(player)
    }

    fun increasePlayerLevel() {
        val newPlayer = player.value!!
        newPlayer.level = newPlayer.level + 1
        updatePlayer(newPlayer)
    }

    fun decreasePlayerLevel() {
        val newPlayer = player.value!!
        newPlayer.level = newPlayer.level - 1
        updatePlayer(newPlayer)
    }

    fun increasePlayerGear() {
        val newPlayer = player.value!!
        newPlayer.gear = newPlayer.gear + 1
        updatePlayer(newPlayer)
    }

    fun decreasePlayerGear() {
        val newPlayer = player.value!!
        newPlayer.gear = newPlayer.gear - 1
        updatePlayer(newPlayer)
    }
}
