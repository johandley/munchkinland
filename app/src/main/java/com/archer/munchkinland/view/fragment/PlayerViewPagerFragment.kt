package com.archer.munchkinland.view.fragment

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.archer.munchkinland.R
import com.archer.munchkinland.adapter.PlayerFragmentViewpagerAdapter
import com.archer.munchkinland.utilities.AddPlayerAlertDialogUtilities
import com.archer.munchkinland.viewmodel.PlayerFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class PlayerViewPagerFragment : Fragment() {
    private val viewModel: PlayerFragmentViewModel by viewModel()
    private lateinit var adapter: PlayerFragmentViewpagerAdapter
    private var startingPosition = 0
    private var positionInitialized = false
    private lateinit var viewPager: ViewPager2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializePosition(this.requireArguments().getInt("startingPosition"))
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        setActionBar()
        return inflater.inflate(R.layout.fragment_player_viewpager, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        positionInitialized = false
        viewPager = requireView().findViewById(R.id.viewpager)
        setActionBarOnClicks()
        adapter = PlayerFragmentViewpagerAdapter(this)
        observePlayers()
        initializeViewPagerAdapter()
    }

    private fun setActionBarOnClicks() {
        val activity = requireActivity() as AppCompatActivity
        val supportActionBar = activity.supportActionBar
        supportActionBar?.customView?.findViewById<ImageButton>(R.id.back_button)?.setOnClickListener {
            activity.onBackPressed()
        }
        supportActionBar?.customView?.findViewById<ImageButton>(R.id.edit_button)?.setOnClickListener {
            launchEditPlayerDialog()
        }
        supportActionBar?.customView?.findViewById<ImageButton>(R.id.revert_button)?.setOnClickListener {
            val player = adapter.getPlayers()[viewPager.currentItem]
            viewModel.resetPlayer(player)
        }
        supportActionBar?.customView?.findViewById<ImageButton>(R.id.death_button)?.setOnClickListener {
            val player = adapter.getPlayers()[viewPager.currentItem]
            viewModel.killPlayer(player)
        }
    }

    private fun setActionBar() {
        val activity = requireActivity() as AppCompatActivity
        val supportActionBar = activity.supportActionBar
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setCustomView(R.layout.player_fragment_custom_action_bar)
    }

    private fun observePlayers() {
        viewModel.getAllPlayersOrderByPositionId().observe(viewLifecycleOwner) {
            adapter.setPlayers(it)
            if (!positionInitialized) {
                viewPager.setCurrentItem(startingPosition, false)
                positionInitialized = true
            }
            registerViewPagerCallback(adapter.getPlayers().size)
        }
    }

    private fun initializeViewPagerAdapter() {
        val viewPager = requireView().findViewById<ViewPager2>(R.id.viewpager)
        viewPager.adapter = adapter
    }

    private fun registerViewPagerCallback(listSize: Int) {
        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageScrollStateChanged(state: Int) {
                super.onPageScrollStateChanged(state)
                startingPosition = viewPager.currentItem
                if (state == ViewPager2.SCROLL_STATE_IDLE) {
                    when (viewPager.currentItem) {
                        listSize - 1 -> viewPager.setCurrentItem(1, false)
                        0 -> viewPager.setCurrentItem(listSize - 2, false)
                    }
                    startingPosition = viewPager.currentItem
                }
            }
        })
    }

    private fun launchEditPlayerDialog() {
        val player = adapter.getPlayers()[viewPager.currentItem]
        val inflater = LayoutInflater.from(context)
        val builder = AlertDialog.Builder(requireContext())
        val builderView = inflater.inflate(R.layout.fragment_add_player_dialog, null)
        AddPlayerAlertDialogUtilities.initializeEditPlayerDialogButtons(builder, requireContext())
        builder.setView(builderView)
        val dialog = builder.show()
        AddPlayerAlertDialogUtilities.initializeDefaultValues(dialog, player)
        AddPlayerAlertDialogUtilities.initializeAutoCompleteTextViews(dialog, resources, requireContext())
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            if (!AddPlayerAlertDialogUtilities.hasErrors(dialog)) {
                updatePlayer(dialog)
                dialog.dismiss()
            }
        }
    }

    private fun initializePosition(position: Int) {
        this.startingPosition = position + 1
    }

    private fun updatePlayer(dialog: AlertDialog) {
        val player = adapter.getPlayers()[viewPager.currentItem]
        player.name = AddPlayerAlertDialogUtilities.getName(dialog)
        player.sex = AddPlayerAlertDialogUtilities.getSex(dialog)
        player.race = AddPlayerAlertDialogUtilities.getRace(dialog)
        player.clazz = AddPlayerAlertDialogUtilities.getClass(dialog)
        viewModel.updatePlayer(player)
    }
}
