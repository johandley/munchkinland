package com.archer.munchkinland.view.fragment

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.widget.ImageView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.archer.munchkinland.R
import com.archer.munchkinland.adapter.PlayerListDragAndDropRecyclerViewAdapter
import com.archer.munchkinland.interfaces.OnStartDragListener
import com.archer.munchkinland.model.Player
import com.archer.munchkinland.model.PositionRecord
import com.archer.munchkinland.utilities.AddPlayerAlertDialogUtilities
import com.archer.munchkinland.utilities.ItemReorderCallback
import com.archer.munchkinland.view.activity.MainActivity
import com.archer.munchkinland.viewmodel.PlayerListFragmentViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

private const val DEFAULT_ACTION_BAR = R.layout.player_list_fragment_custom_action_bar

class PlayerListDragAndDropFragment : Fragment(), OnStartDragListener {
    private val viewModel: PlayerListFragmentViewModel by viewModel()
    private val adapter = PlayerListDragAndDropRecyclerViewAdapter(this, this)
    private val callback: ItemTouchHelper.Callback = ItemReorderCallback(adapter)
    private val itemTouchHelper = ItemTouchHelper(callback)

    // removes unwanted animation when switching between drag and drop and list fragment
    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        val a: Animation = object : Animation() {}
        a.duration = 0
        return a
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        setActionBar(DEFAULT_ACTION_BAR)
        return inflater.inflate(R.layout.fragment_player_list_drag_and_drop, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observePlayers()
        initializeAddPlayerFabOnClick()
        setRecyclerViewAdapter()
        setRecyclerViewAdapterItemTouchHelper()
    }

    private fun setRecyclerViewAdapter() {
        val playersRecyclerView =
            requireView().findViewById<RecyclerView>(R.id.players_recyclerview)
        playersRecyclerView.layoutManager = LinearLayoutManager(context)
        playersRecyclerView.adapter = adapter
    }

    private fun initializeAddPlayerFabOnClick() {
        val addPlayerFab = view?.findViewById<FloatingActionButton>(R.id.add_player_fab)
        addPlayerFab?.setOnClickListener {
            launchNewPlayerDialog()
        }
    }

    private fun setActionBar(actionBarId: Int) {
        val activity = requireActivity() as AppCompatActivity
        val supportActionBar = activity.supportActionBar
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setCustomView(actionBarId)
        setDefaultActionBarListeners()
    }

    private fun setRecyclerViewAdapterItemTouchHelper() {
        val playersRecyclerView =
            requireView().findViewById<RecyclerView>(R.id.players_recyclerview)
        itemTouchHelper.attachToRecyclerView(playersRecyclerView)
    }

    private fun setDefaultActionBarListeners() {
        val activity = requireActivity() as MainActivity
        val supportActionBar = activity.supportActionBar
        supportActionBar?.customView?.findViewById<ImageView>(R.id.edit_button)?.setOnClickListener {
            activity.inflateFragment(PlayerListFragment(), null, "PLAYER_LIST_FRAGMENT", false)
        }
    }

    private fun launchNewPlayerDialog() {
        val inflater = LayoutInflater.from(context)
        val builder = AlertDialog.Builder(context)
        val builderView = inflater.inflate(R.layout.fragment_add_player_dialog, null)
        AddPlayerAlertDialogUtilities.initializeAddPlayerDialogButtons(builder, requireContext())
        builder.setView(builderView)
        val dialog = builder.show()
        AddPlayerAlertDialogUtilities.initializeAutoCompleteTextViews(dialog, resources, requireContext())
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            if (!AddPlayerAlertDialogUtilities.hasErrors(dialog)) {
                createPlayer(dialog)
                dialog.dismiss()
            }
        }
    }

    private fun createPlayer(dialog: AlertDialog) {
        CoroutineScope(Dispatchers.IO).launch {
            val positionRecordId = viewModel.createPositionRecord(PositionRecord(0))
            val player = Player(
                0,
                positionRecordId,
                AddPlayerAlertDialogUtilities.getName(dialog),
                AddPlayerAlertDialogUtilities.getSex(dialog),
                AddPlayerAlertDialogUtilities.getRace(dialog),
                AddPlayerAlertDialogUtilities.getClass(dialog),
                1,
                0
            )
            viewModel.addPlayer(player)
        }
    }

    fun launchEditPlayerDialog(player: Player) {
        val inflater = LayoutInflater.from(context)
        val builder = AlertDialog.Builder(context)
        val builderView = inflater.inflate(R.layout.fragment_add_player_dialog, null)
        AddPlayerAlertDialogUtilities.initializeEditPlayerDialogButtons(builder, requireContext())
        builder.setView(builderView)
        val dialog = builder.show()
        AddPlayerAlertDialogUtilities.initializeDefaultValues(dialog, player)
        AddPlayerAlertDialogUtilities.initializeAutoCompleteTextViews(dialog, resources, requireContext())
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            if (!AddPlayerAlertDialogUtilities.hasErrors(dialog)) {
                updatePlayer(dialog, player)
                dialog.dismiss()
            }
        }
    }

    private fun updatePlayer(dialog: AlertDialog, player: Player) {
        val newPlayer = player.copy()
        newPlayer.name = AddPlayerAlertDialogUtilities.getName(dialog)
        newPlayer.sex = AddPlayerAlertDialogUtilities.getSex(dialog)
        newPlayer.race = AddPlayerAlertDialogUtilities.getRace(dialog)
        newPlayer.clazz = AddPlayerAlertDialogUtilities.getClass(dialog)
        CoroutineScope(Dispatchers.IO).launch {
            viewModel.updatePlayer(newPlayer)
        }
    }

    override fun onStartDrag(viewHolder: RecyclerView.ViewHolder?) {
        viewHolder?.let {
            itemTouchHelper.startDrag(it)
        }
    }

    fun swapPositionRecords(fromPosition: Int, toPosition: Int) {
        viewModel.swapPlayerPositionRecords(fromPosition, toPosition)
    }

    private fun observePlayers() {
        // this is here since the view model is created lazily the first time it is accessed the view model
        // must be created on the main thread therefore we "access" it prior to calling the coroutine
        viewModel
        CoroutineScope(Dispatchers.IO).launch {
            val players = viewModel.getAllPlayersNonLiveOrderByPositionId()
            adapter.setPlayers(players)
        }
    }
}
