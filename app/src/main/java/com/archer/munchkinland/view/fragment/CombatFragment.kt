package com.archer.munchkinland.view.fragment

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.archer.munchkinland.R
import com.archer.munchkinland.adapter.AddPlayerToCombatDialogFragmentRecyclerViewAdapter
import com.archer.munchkinland.adapter.CombatFragmentMonsterRecyclerViewAdapter
import com.archer.munchkinland.adapter.CombatFragmentPlayerRecyclerViewAdapter
import com.archer.munchkinland.model.Player
import com.archer.munchkinland.viewmodel.CombatFragmentViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class CombatFragment : Fragment() {
    private val viewModel: CombatFragmentViewModel by viewModel()
    private val playerAdapter = CombatFragmentPlayerRecyclerViewAdapter(this)
    private val monsterAdapter = CombatFragmentMonsterRecyclerViewAdapter(this)
    private val addPlayerToCombatDialogAdapter = AddPlayerToCombatDialogFragmentRecyclerViewAdapter(this)
    private lateinit var addPlayerToCombatAlertDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeMonsters()
        val bundle = arguments
        if (bundle != null) {
            val playerId = bundle.getLong("playerId")
            initializePlayers(playerId)
            initializePlayersNotInCombat(playerId)
        } else {
            Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        setActionBar()
        return inflater.inflate(R.layout.fragment_combat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observePlayers()
        observeMonsters()
        observePlayerCombatStrength()
        observeMonsterCombatStrength()
        setRecyclerViewAdapters()
        setFloatingActionButtonOnClickListeners()
        setActionBarOnClicks()
    }

    private fun setActionBar() {
        val activity = requireActivity() as AppCompatActivity
        val supportActionBar = activity.supportActionBar
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setCustomView(R.layout.combat_fragment_custom_action_bar)
    }

    private fun setActionBarOnClicks() {
        val activity = requireActivity() as AppCompatActivity
        val supportActionBar = activity.supportActionBar
        supportActionBar?.customView?.findViewById<ImageButton>(R.id.back_button)?.setOnClickListener {
            activity.onBackPressed()
        }
        supportActionBar?.customView?.findViewById<ImageButton>(R.id.end_combat_button)?.setOnClickListener {
            viewModel.resolveCombat()
            activity.onBackPressed()
        }
    }

    private fun setFloatingActionButtonOnClickListeners() {
        val addPlayerOrMonsterFloatingActionButton = requireView().findViewById<FloatingActionButton>(R.id.add_player_or_monster_fab)
        val addPlayerFloatingActionButton = requireView().findViewById<FloatingActionButton>(R.id.add_player_fab)
        val addMonsterFloatingActionButton = requireView().findViewById<FloatingActionButton>(R.id.add_monster_fab)
        addPlayerOrMonsterFloatingActionButton.setOnClickListener {
            showOrHideMiniFloatingActionButtons()
        }
        addPlayerFloatingActionButton.setOnClickListener {
            showAddPlayerToCombatDialog()
            hideMiniFloatingActionButtons()
        }
        addMonsterFloatingActionButton.setOnClickListener {
            viewModel.addMonster()
            hideMiniFloatingActionButtons()
        }
    }

    private fun showOrHideMiniFloatingActionButtons() {
        val addPlayerFloatingActionButton = requireView().findViewById<FloatingActionButton>(R.id.add_player_fab)
        val addMonsterFloatingActionButton = requireView().findViewById<FloatingActionButton>(R.id.add_monster_fab)
        if (addPlayerFloatingActionButton.visibility == View.VISIBLE && addMonsterFloatingActionButton.visibility == View.VISIBLE) {
            hideMiniFloatingActionButtons()
        } else {
            showMiniFloatingActionButtons()
        }
    }

    private fun hideMiniFloatingActionButtons() {
        val addPlayerFloatingActionButton = requireView().findViewById<FloatingActionButton>(R.id.add_player_fab)
        val addMonsterFloatingActionButton = requireView().findViewById<FloatingActionButton>(R.id.add_monster_fab)
        addPlayerFloatingActionButton.hide()
        addMonsterFloatingActionButton.hide()
    }

    private fun showMiniFloatingActionButtons() {
        val addPlayerFloatingActionButton = requireView().findViewById<FloatingActionButton>(R.id.add_player_fab)
        val addMonsterFloatingActionButton = requireView().findViewById<FloatingActionButton>(R.id.add_monster_fab)
        addPlayerFloatingActionButton.show()
        addMonsterFloatingActionButton.show()
    }

    private fun showAddPlayerToCombatDialog() {
        val inflater = LayoutInflater.from(context)
        val builder = AlertDialog.Builder(context)
        val builderView = inflater.inflate(R.layout.add_player_to_combat_dialog, null)
        builder.setView(builderView)
        val dialog = builder.show()
        addPlayerToCombatAlertDialog = dialog
        setDialogFragmentRecyclerViewAdapter(dialog)
    }

    fun hideAddPlayerToCombatDialog() {
        addPlayerToCombatAlertDialog.hide()
    }

    private fun setDialogFragmentRecyclerViewAdapter(dialog: AlertDialog) {
        val addPlayerToCombatRecyclerView = dialog.findViewById<RecyclerView>(R.id.players_recyclerview)
        addPlayerToCombatRecyclerView.adapter = addPlayerToCombatDialogAdapter
        addPlayerToCombatRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }

    fun addPlayer(id: Long) = viewModel.addPlayer(id)

    private fun initializePlayers(playerId: Long) {
        viewModel.initializePlayers(playerId)
    }

    private fun initializeMonsters() {
        viewModel.initializeMonsters()
    }

    private fun initializePlayersNotInCombat(id: Long) {
        CoroutineScope(Dispatchers.IO).launch {
            val playersNotInCombat = viewModel.getPlayersNotInCombat(id)
            addPlayerToCombatDialogAdapter.setPlayers(playersNotInCombat)
        }
    }

    private fun setRecyclerViewAdapters() {
        val playerCombatRecyclerView = requireView().findViewById<RecyclerView>(R.id.player_combat_recyclerview)
        playerCombatRecyclerView.adapter = playerAdapter
        playerCombatRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        val monsterCombatRecyclerView = requireView().findViewById<RecyclerView>(R.id.monster_combat_recyclerview)
        monsterCombatRecyclerView.adapter = monsterAdapter
        monsterCombatRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    }

    fun increaseCombinedPlayerCombatStrength() = viewModel.increaseCombinedPlayerCombatStrength()

    fun decreaseCombinedPlayerCombatStrength() = viewModel.decreaseCombinedPlayerCombatStrength()

    fun increaseCombinedMonsterCombatStrength() = viewModel.increaseCombinedMonsterCombatStrength()

    fun decreaseCombinedMonsterCombatStrength() = viewModel.decreaseCombinedMonsterCombatStrength()

    fun increasePlayerLevel(player: Player) = viewModel.increasePlayerLevel(player)

    fun decreasePlayerLevel(player: Player) = viewModel.decreasePlayerLevel(player)

    fun increasePlayerGear(player: Player) = viewModel.increasePlayerGear(player)

    fun decreasePlayerGear(player: Player) = viewModel.decreasePlayerGear(player)

    fun removePlayerFromCombat(player: Player, modifier: Int) = viewModel.removePlayerFromCombat(player, modifier)

    fun increaseMonsterLevel(position: Int) = viewModel.increaseMonsterLevel(position)

    fun decreaseMonsterLevel(position: Int) = viewModel.decreaseMonsterLevel(position)

    fun increaseMonsterModifier(position: Int) = viewModel.increaseMonsterModifier(position)

    fun decreaseMonsterModifier(position: Int) = viewModel.decreaseMonsterModifier(position)

    fun removeMonsterFromCombat(position: Int) = viewModel.removeMonsterFromCombat(position)

    private fun observePlayers() {
        viewModel.players.observe(viewLifecycleOwner) {
            playerAdapter.setPlayers(it)
        }
    }

    private fun observeMonsters() {
        viewModel.monsters.observe(viewLifecycleOwner) {
            monsterAdapter.setMonsters(it)
        }
    }

    private fun observePlayerCombatStrength() {
        viewModel.playerCombinedCombatStrength.observe(viewLifecycleOwner) {
            val playerCombatStrengthCombinedTextView = requireView().findViewById<TextView>(R.id.player_combat_strength_combined_textview)
            playerCombatStrengthCombinedTextView.text = it.toString()
        }
    }

    private fun observeMonsterCombatStrength() {
        viewModel.monsterCombinedCombatStrength.observe(viewLifecycleOwner) {
            val monsterCombatStrengthCombinedTextView = requireView().findViewById<TextView>(R.id.monster_combat_strength_combined_textview)
            monsterCombatStrengthCombinedTextView.text = it.toString()
        }
    }
}
