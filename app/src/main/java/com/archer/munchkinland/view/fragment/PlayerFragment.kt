package com.archer.munchkinland.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.archer.munchkinland.R
import com.archer.munchkinland.model.Player
import com.archer.munchkinland.view.activity.MainActivity
import com.archer.munchkinland.viewmodel.PlayerFragmentViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import org.koin.androidx.viewmodel.ext.android.viewModel

class PlayerFragment(private val playerId: Long) : Fragment() {
    private val viewModel: PlayerFragmentViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.initializePlayer(playerId)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_player, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeObservers()
        initializeTextViewOnClicks()
        initializeFloatingActionButtonOnClick()
    }

    private fun initializeObservers() {
        viewModel.player.observe(viewLifecycleOwner) {
            initializeTextViewValues(it)
        }
    }

    private fun initializeTextViewValues(player: Player) {
        val playerNameTextView = requireView().findViewById<TextView>(R.id.player_name_textview)
        val playerClassTextView = requireView().findViewById<TextView>(R.id.player_class_textview)
        val playerRaceTextView = requireView().findViewById<TextView>(R.id.player_race_textview)
        val playerCombatStrengthTextView = requireView().findViewById<TextView>(R.id.player_combat_strength_value_textview)
        val playerGearTextView = requireView().findViewById<TextView>(R.id.player_gear_value_textview)
        val playerLevelTextView = requireView().findViewById<TextView>(R.id.player_level_value_textview)
        playerNameTextView?.text = player.name
        playerClassTextView?.text = player.clazz
        playerRaceTextView?.text = player.race
        playerGearTextView?.text = player.gear.toString()
        playerLevelTextView?.text = player.level.toString()
        val combatStrength = player.gear + player.level
        playerCombatStrengthTextView.text = combatStrength.toString()
    }

    private fun initializeFloatingActionButtonOnClick() {
        val startCombatButton = requireView().findViewById<FloatingActionButton>(R.id.start_combat_fab)
        startCombatButton.setOnClickListener {
            val parentActivity = requireActivity() as MainActivity
            val bundle = Bundle()
            bundle.putLong("playerId", playerId)
            parentActivity.inflateFragment(CombatFragment(), bundle, "COMBAT_FRAGMENT", true)
        }
    }

    private fun initializeTextViewOnClicks() {
        initializeDecreasePlayerGearTextViewOnClick()
        initializeIncreasePlayerGearTextViewOnClick()
        initializeDecreasePlayerLevelTextViewOnClick()
        initializeIncreasePlayerLevelTextViewOnClick()
    }

    private fun initializeIncreasePlayerLevelTextViewOnClick() {
        val increasePlayerLevelTextView = requireView().findViewById<TextView>(R.id.increase_player_level_textview)
        increasePlayerLevelTextView.setOnClickListener {
            viewModel.increasePlayerLevel()
        }
    }

    private fun initializeDecreasePlayerLevelTextViewOnClick() {
        val decreasePlayerLevelTextView = requireView().findViewById<TextView>(R.id.decrease_player_level_textview)
        decreasePlayerLevelTextView.setOnClickListener {
            viewModel.decreasePlayerLevel()
        }
    }

    private fun initializeIncreasePlayerGearTextViewOnClick() {
        val increasePlayerGearTextView = requireView().findViewById<TextView>(R.id.increase_player_gear_textview)
        increasePlayerGearTextView.setOnClickListener {
            viewModel.increasePlayerGear()
        }
    }

    private fun initializeDecreasePlayerGearTextViewOnClick() {
        val decreasePlayerGearTextView = requireView().findViewById<TextView>(R.id.decrease_player_gear_textview)
        decreasePlayerGearTextView.setOnClickListener {
            viewModel.decreasePlayerGear()
        }
    }
}
