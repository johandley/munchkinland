package com.archer.munchkinland.view.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.archer.munchkinland.R
import com.archer.munchkinland.view.fragment.PlayerListFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        inflateFragment(PlayerListFragment(), null, "PLAYER_LIST_FRAGMENT", false)
    }

    fun inflateFragment(fragment: Fragment, bundle: Bundle?, tag: String, addToBackStack: Boolean) {
        if (bundle != null) {
            fragment.arguments = bundle
        }
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, fragment, tag)
        if (addToBackStack) transaction.addToBackStack(tag)
        transaction.commit()
    }
}
