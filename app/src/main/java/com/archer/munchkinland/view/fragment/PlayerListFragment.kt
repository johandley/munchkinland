package com.archer.munchkinland.view.fragment

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.widget.ImageButton
import android.widget.ImageView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.recyclerview.selection.SelectionPredicates
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.StorageStrategy
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.archer.munchkinland.R
import com.archer.munchkinland.adapter.PlayerListRecyclerViewAdapter
import com.archer.munchkinland.adapter.helpers.PlayerListRecyclerViewAdapterItemDetailsLookup
import com.archer.munchkinland.adapter.helpers.PlayerListRecyclerViewAdapterItemKeyProvider
import com.archer.munchkinland.model.Player
import com.archer.munchkinland.model.PositionRecord
import com.archer.munchkinland.utilities.AddPlayerAlertDialogUtilities
import com.archer.munchkinland.view.activity.MainActivity
import com.archer.munchkinland.viewmodel.PlayerListFragmentViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

private const val SELECTION_ACTION_BAR = R.layout.player_list_fragment_selection_custom_action_bar
private const val DEFAULT_ACTION_BAR = R.layout.player_list_fragment_custom_action_bar

class PlayerListFragment : Fragment() {
    private val viewModel: PlayerListFragmentViewModel by viewModel()
    private val adapter = PlayerListRecyclerViewAdapter()
    private var inSelectionMenu = false

    // removes unwanted animation when switching between drag and drop and list fragment
    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        val a: Animation = object : Animation() {}
        a.duration = 0
        return a
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        setActionBar(DEFAULT_ACTION_BAR)
        return inflater.inflate(R.layout.fragment_player_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observePlayers()
        initializeAddPlayerFabOnClick()
        setDefaultRecyclerViewAdapter()
    }

    private fun setDefaultRecyclerViewAdapter() {
        val playersRecyclerView =
            requireView().findViewById<RecyclerView>(R.id.players_recyclerview)
        playersRecyclerView.itemAnimator = null
        playersRecyclerView.layoutManager = LinearLayoutManager(context)
        playersRecyclerView.adapter = adapter
        initializeRecyclerViewTracker(playersRecyclerView)
    }

    private fun initializeAddPlayerFabOnClick() {
        val addPlayerFab = view?.findViewById<FloatingActionButton>(R.id.add_player_fab)
        addPlayerFab?.setOnClickListener {
            launchNewPlayerDialog()
        }
    }

    private fun initializeRecyclerViewTracker(recyclerView: RecyclerView) {
        val tracker = SelectionTracker.Builder(
            "mySelection",
            recyclerView,
            PlayerListRecyclerViewAdapterItemKeyProvider(recyclerView),
            PlayerListRecyclerViewAdapterItemDetailsLookup(recyclerView),
            StorageStrategy.createLongStorage()
        ).withSelectionPredicate(
            SelectionPredicates.createSelectAnything()
        ).build()
        adapter.tracker = tracker
        initializeRecyclerViewTrackerObserver(tracker)
    }

    private fun initializeRecyclerViewTrackerObserver(tracker: SelectionTracker<Long>) {
        tracker.addObserver(
            object : SelectionTracker.SelectionObserver<Long>() {
                override fun onSelectionChanged() {
                    super.onSelectionChanged()
                    if (!inSelectionMenu) inSelectionMenu = true
                    setActionBar(tracker)
                }
            }
        )
    }

    private fun setActionBar(actionBarId: Int) {
        val activity = requireActivity() as AppCompatActivity
        val supportActionBar = activity.supportActionBar
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setCustomView(actionBarId)
        setDefaultActionBarListeners()
    }

    private fun setActionBar(tracker: SelectionTracker<Long>) {
        if (inSelectionMenu && tracker.selection.isEmpty) {
            setActionBar(DEFAULT_ACTION_BAR)
        } else if (inSelectionMenu && !tracker.selection.isEmpty) {
            setActionBar(SELECTION_ACTION_BAR)
            setSelectedActionBarListeners(tracker)
        }
    }

    private fun setDefaultActionBarListeners() {
        val activity = requireActivity() as MainActivity
        val supportActionBar = activity.supportActionBar
        supportActionBar?.customView?.findViewById<ImageView>(R.id.edit_button)?.setOnClickListener {
            activity.inflateFragment(PlayerListDragAndDropFragment(), null, "PLAYER_LIST_DRAG_DROP_FRAGMENT", true)
        }
    }

    private fun setSelectedActionBarListeners(tracker: SelectionTracker<Long>) {
        setSelectionActionBarDeleteButtonListener(tracker)
    }

    private fun setSelectionActionBarDeleteButtonListener(tracker: SelectionTracker<Long>) {
        val activity = requireActivity() as AppCompatActivity
        activity.supportActionBar?.customView?.findViewById<ImageButton>(R.id.delete_button)?.setOnClickListener {
            setActionBar(DEFAULT_ACTION_BAR)
            deleteSelectedPlayers()
            tracker.clearSelection()
        }
    }

    private fun deleteSelectedPlayers() {
        val activity = requireActivity() as AppCompatActivity
        val recyclerView = activity.findViewById<RecyclerView>(R.id.players_recyclerview)
        recyclerView.children.forEach {
            if (it.isActivated) {
                CoroutineScope(Dispatchers.IO).launch {
                    viewModel.deletePlayer(it.tag.toString().toLong())
                }
            }
        }
    }

    private fun launchNewPlayerDialog() {
        val inflater = LayoutInflater.from(context)
        val builder = AlertDialog.Builder(context)
        val builderView = inflater.inflate(R.layout.fragment_add_player_dialog, null)
        AddPlayerAlertDialogUtilities.initializeAddPlayerDialogButtons(builder, requireContext())
        builder.setView(builderView)
        val dialog = builder.show()
        AddPlayerAlertDialogUtilities.initializeAutoCompleteTextViews(dialog, resources, requireContext())
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            if (!AddPlayerAlertDialogUtilities.hasErrors(dialog)) {
                createPlayer(dialog)
                dialog.dismiss()
            }
        }
    }

    private fun createPlayer(dialog: AlertDialog) {
        CoroutineScope(Dispatchers.IO).launch {
            val positionRecordId = viewModel.createPositionRecord(PositionRecord(0))
            val player = Player(
                0,
                positionRecordId,
                AddPlayerAlertDialogUtilities.getName(dialog),
                AddPlayerAlertDialogUtilities.getSex(dialog),
                AddPlayerAlertDialogUtilities.getRace(dialog),
                AddPlayerAlertDialogUtilities.getClass(dialog),
                1,
                0
            )
            viewModel.addPlayer(player)
        }
    }

    private fun observePlayers() {
        viewModel.getAllPlayersOrderByPositionId().observe(viewLifecycleOwner) {
            adapter.setPlayers(it)
        }
    }
}
