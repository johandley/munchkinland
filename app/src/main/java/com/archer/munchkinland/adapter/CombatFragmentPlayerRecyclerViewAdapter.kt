package com.archer.munchkinland.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.archer.munchkinland.R
import com.archer.munchkinland.model.Player
import com.archer.munchkinland.view.fragment.CombatFragment
import java.util.*

class CombatFragmentPlayerRecyclerViewAdapter(private val parentFragment: CombatFragment) :
    RecyclerView.Adapter<CombatFragmentPlayerRecyclerViewAdapter.ViewHolder>() {
    private var players: List<Player> = ArrayList()
    private lateinit var recyclerView: RecyclerView

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.player_combat_recyclerview_item, parent, false)
        return ViewHolder(view)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(players[position])
    }

    override fun getItemCount(): Int {
        return players.size
    }

    override fun getItemId(position: Int): Long = position.toLong()

    @SuppressLint("NotifyDataSetChanged")
    fun setPlayers(newDataSet: List<Player>) {
        this.players = newDataSet
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(player: Player) {
            val playerNameTextView = itemView.findViewById<TextView>(R.id.player_name_textview)
            val playerLevelTextView = itemView.findViewById<TextView>(R.id.player_level_value_textview)
            val playerGearTextView = itemView.findViewById<TextView>(R.id.player_gear_value_textview)
            val playerModifierTextView = itemView.findViewById<TextView>(R.id.player_modifier_value_textview)
            playerNameTextView.text = player.name
            playerLevelTextView.text = player.level.toString()
            playerGearTextView.text = player.gear.toString()
            if (playerModifierTextView.text.isBlank()) {
                playerModifierTextView.text = "0"
            }
            initializeTextViewOnClicks(player)

            val deleteButton = itemView.findViewById<ImageButton>(R.id.delete_button)
            if (adapterPosition == 0) deleteButton.visibility = View.GONE
            initializeDeleteButtonOnClick(player)
        }

        private fun initializeTextViewOnClicks(player: Player) {
            initializeDecreasePlayerGearTextViewOnClick(player)
            initializeIncreasePlayerGearTextViewOnClick(player)
            initializeDecreasePlayerLevelTextViewOnClick(player)
            initializeIncreasePlayerLevelTextViewOnClick(player)
            initializeDecreasePlayerModifierTextViewOnClick()
            initializeIncreasePlayerModifierTextViewOnClick()
        }

        private fun initializeIncreasePlayerLevelTextViewOnClick(player: Player) {
            val increasePlayerLevelTextView = itemView.findViewById<TextView>(R.id.increase_player_level_textview)
            increasePlayerLevelTextView.setOnClickListener {
                parentFragment.increasePlayerLevel(player)
                parentFragment.increaseCombinedPlayerCombatStrength()
            }
        }

        private fun initializeDecreasePlayerLevelTextViewOnClick(player: Player) {
            val decreasePlayerLevelTextView = itemView.findViewById<TextView>(R.id.decrease_player_level_textview)
            decreasePlayerLevelTextView.setOnClickListener {
                parentFragment.decreasePlayerLevel(player)
                parentFragment.decreaseCombinedPlayerCombatStrength()
            }
        }

        private fun initializeIncreasePlayerGearTextViewOnClick(player: Player) {
            val increasePlayerGearTextView = itemView.findViewById<TextView>(R.id.increase_player_gear_textview)
            increasePlayerGearTextView.setOnClickListener {
                parentFragment.increasePlayerGear(player)
                parentFragment.increaseCombinedPlayerCombatStrength()
            }
        }

        private fun initializeDecreasePlayerGearTextViewOnClick(player: Player) {
            val decreasePlayerGearTextView = itemView.findViewById<TextView>(R.id.decrease_player_gear_textview)
            decreasePlayerGearTextView.setOnClickListener {
                parentFragment.decreasePlayerGear(player)
                parentFragment.decreaseCombinedPlayerCombatStrength()
            }
        }

        private fun initializeIncreasePlayerModifierTextViewOnClick() {
            val increasePlayerModifierTextView = itemView.findViewById<TextView>(R.id.increase_player_modifier_textview)
            val playerModifierValueTextView = itemView.findViewById<TextView>(R.id.player_modifier_value_textview)
            increasePlayerModifierTextView.setOnClickListener {
                val currentModifier = playerModifierValueTextView.text.toString().toInt()
                val newModifier = currentModifier + 1
                playerModifierValueTextView.text = newModifier.toString()
                parentFragment.increaseCombinedPlayerCombatStrength()
            }
        }

        private fun initializeDecreasePlayerModifierTextViewOnClick() {
            val decreasePlayerModifierTextView = itemView.findViewById<TextView>(R.id.decrease_player_modifier_textview)
            val playerModifierValueTextView = itemView.findViewById<TextView>(R.id.player_modifier_value_textview)
            decreasePlayerModifierTextView.setOnClickListener {
                val currentModifier = playerModifierValueTextView.text.toString().toInt()
                val newModifier = currentModifier - 1
                playerModifierValueTextView.text = newModifier.toString()
                parentFragment.decreaseCombinedPlayerCombatStrength()
            }
        }

        private fun initializeDeleteButtonOnClick(player: Player) {
            val deleteButton = itemView.findViewById<ImageButton>(R.id.delete_button)
            deleteButton.setOnClickListener {
                val playerModifierTextView = itemView.findViewById<TextView>(R.id.player_modifier_value_textview)
                parentFragment.removePlayerFromCombat(player, playerModifierTextView.text.toString().toInt())
                playerModifierTextView.text = "0"
            }
        }
    }
}
