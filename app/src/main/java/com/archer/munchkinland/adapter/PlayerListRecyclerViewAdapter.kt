package com.archer.munchkinland.adapter

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.RecyclerView
import com.archer.munchkinland.R
import com.archer.munchkinland.model.Player
import com.archer.munchkinland.view.activity.MainActivity
import com.archer.munchkinland.view.fragment.PlayerViewPagerFragment
import java.util.*

class PlayerListRecyclerViewAdapter :
    RecyclerView.Adapter<PlayerListRecyclerViewAdapter.ViewHolder>() {
    private var players: List<Player> = ArrayList()
    private lateinit var recyclerView: RecyclerView
    var tracker: SelectionTracker<Long>? = null

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.player_list_recyclerview_item, parent, false)
        return ViewHolder(view)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        tracker?.let {
            holder.bindItems(players[position], it.isSelected(position.toLong()))
        }
    }

    override fun getItemCount(): Int {
        return players.size
    }

    override fun getItemId(position: Int): Long = position.toLong()

    @SuppressLint("NotifyDataSetChanged")
    fun setPlayers(newDataSet: List<Player>) {
        this.players = newDataSet
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun getItemDetails(): ItemDetailsLookup.ItemDetails<Long> =
            object : ItemDetailsLookup.ItemDetails<Long>() {
                override fun getPosition(): Int = adapterPosition
                override fun getSelectionKey(): Long = itemId
            }

        fun bindItems(player: Player, isActivated: Boolean) {
            itemView.isActivated = isActivated
            itemView.tag = player.id.toInt()
            val sexImageView = itemView.findViewById<ImageView>(R.id.sex_imageview)
            val playerNameTextview = itemView.findViewById<TextView>(R.id.player_name_textview)
            val raceAndClassTextview = itemView.findViewById<TextView>(R.id.race_and_class_textview)
            val gearTextView = itemView.findViewById<TextView>(R.id.gear_textview)
            val levelTextView = itemView.findViewById<TextView>(R.id.level_textview)
            val combatStrengthTextView = itemView.findViewById<TextView>(R.id.combat_strength_textview)

            val raceAndClassText = "${player.race} ${player.clazz}"
            val combatStrength = player.gear + player.level

            playerNameTextview.text = player.name
            raceAndClassTextview.text = raceAndClassText
            gearTextView.text = player.gear.toString()
            levelTextView.text = player.level.toString()
            combatStrengthTextView.text = combatStrength.toString()

            if (player.sex == "Male") {
                sexImageView.setBackgroundResource(R.drawable.male_recycler_view_item_selector)
            } else {
                sexImageView.setBackgroundResource(R.drawable.female_recycler_view_item_selector)
            }

            setOnClickListener()
        }

        private fun setOnClickListener() {
            itemView.setOnClickListener {
                val parentActivity = itemView.context as MainActivity
                val bundle = Bundle()
                bundle.putInt("startingPosition", adapterPosition)
                parentActivity.inflateFragment(PlayerViewPagerFragment(), bundle, "PLAYER_VIEW_PAGER_FRAGMENT", true)
            }
        }
    }
}
