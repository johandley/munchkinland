package com.archer.munchkinland.adapter

import android.annotation.SuppressLint
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.adapter.FragmentViewHolder
import com.archer.munchkinland.R
import com.archer.munchkinland.model.Player
import com.archer.munchkinland.view.fragment.PlayerFragment
import com.archer.munchkinland.view.fragment.PlayerViewPagerFragment

class PlayerFragmentViewpagerAdapter(
    parentFragment: PlayerViewPagerFragment
) : FragmentStateAdapter(parentFragment) {
    private var players: List<Player> = ArrayList()

    override fun createFragment(position: Int): Fragment {
        return PlayerFragment(players[position].id)
    }

    override fun getItemId(position: Int): Long {
        return players[position].id
    }

    override fun containsItem(itemId: Long): Boolean = players.any { it.id == itemId }

    override fun getItemCount(): Int {
        return players.size
    }

    fun getPlayers(): List<Player> {
        return players
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setPlayers(newDataSet: List<Player>) {
        this.players = listOf(newDataSet.last()) + newDataSet + listOf(newDataSet.first())
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: FragmentViewHolder, position: Int, payloads: MutableList<Any>) {
        bindTextViewData(holder.itemView, players[position])
        super.onBindViewHolder(holder, position, payloads)
    }

    private fun bindTextViewData(itemView: View, player: Player) {
        val playerNameTextView = itemView.findViewById<TextView>(R.id.player_name_textview)
        val playerClassTextView = itemView.findViewById<TextView>(R.id.player_class_textview)
        val playerRaceTextView = itemView.findViewById<TextView>(R.id.player_race_textview)
        val playerCombatStrengthTextView = itemView.findViewById<TextView>(R.id.player_combat_strength_value_textview)
        val playerGearTextView = itemView.findViewById<TextView>(R.id.player_gear_value_textview)
        val playerLevelTextView = itemView.findViewById<TextView>(R.id.player_level_value_textview)
        playerNameTextView?.text = player.name
        playerClassTextView?.text = player.clazz
        playerRaceTextView?.text = player.race
        playerGearTextView?.text = player.gear.toString()
        playerLevelTextView?.text = player.level.toString()
        val combatStrength = player.gear + player.level
        playerCombatStrengthTextView?.text = combatStrength.toString()
    }
}
