package com.archer.munchkinland.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.archer.munchkinland.R
import com.archer.munchkinland.interfaces.ItemTouchHelperAdapter
import com.archer.munchkinland.interfaces.OnStartDragListener
import com.archer.munchkinland.model.Player
import com.archer.munchkinland.view.fragment.PlayerListDragAndDropFragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

class PlayerListDragAndDropRecyclerViewAdapter(
    private val onStartDragListener: OnStartDragListener,
    private val parentFragment: PlayerListDragAndDropFragment
) :
    RecyclerView.Adapter<PlayerListDragAndDropRecyclerViewAdapter.ViewHolder>(),
    ItemTouchHelperAdapter {
    private var players: List<Player> = ArrayList()
    private lateinit var recyclerView: RecyclerView

    init {
        setHasStableIds(false)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerListDragAndDropRecyclerViewAdapter.ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.player_list_drag_and_drop_recyclerview_item, parent, false)
        return ViewHolder(view, onStartDragListener)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onBindViewHolder(holder: PlayerListDragAndDropRecyclerViewAdapter.ViewHolder, position: Int) {
        holder.bindItems(players[position])
    }

    override fun getItemCount(): Int {
        return players.size
    }

    override fun getItemId(position: Int): Long = position.toLong()

    @SuppressLint("NotifyDataSetChanged")
    fun setPlayers(newDataSet: List<Player>) {
        this.players = newDataSet
        notifyDataSetChanged()
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(players, i, i + 1)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(players, i, i - 1)
            }
        }
        notifyItemMoved(fromPosition, toPosition)
        return true
    }

    override fun swapPositionRecords(fromPosition: Int, toPosition: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            parentFragment.swapPositionRecords(fromPosition, toPosition)
        }
        notifyItemRangeChanged(fromPosition, toPosition, false)
    }

    override fun onItemDismiss(position: Int) {
    }

    inner class ViewHolder(itemView: View, private val onStartDragListener: OnStartDragListener? = null) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(player: Player) {
            itemView.tag = player.id.toInt()
            val sexImageView = itemView.findViewById<ImageView>(R.id.sex_imageview)
            val playerNameTextview = itemView.findViewById<TextView>(R.id.player_name_textview)
            val raceAndClassTextview = itemView.findViewById<TextView>(R.id.race_and_class_textview)
            val gearTextView = itemView.findViewById<TextView>(R.id.gear_textview)
            val levelTextView = itemView.findViewById<TextView>(R.id.level_textview)
            val combatStrengthTextView = itemView.findViewById<TextView>(R.id.combat_strength_textview)

            val raceAndClassText = "${player.race} ${player.clazz}"
            val combatStrength = player.gear + player.level

            playerNameTextview.text = player.name
            raceAndClassTextview.text = raceAndClassText
            gearTextView.text = player.gear.toString()
            levelTextView.text = player.level.toString()
            combatStrengthTextView.text = combatStrength.toString()

            if (player.sex == "Male") {
                sexImageView.setBackgroundResource(R.drawable.male_recycler_view_item_selector)
            } else {
                sexImageView.setBackgroundResource(R.drawable.female_recycler_view_item_selector)
            }

            setOnClickListener(player)
            setOnTouchListener()
        }

        private fun setOnClickListener(player: Player) {
            itemView.setOnClickListener {
                parentFragment.launchEditPlayerDialog(player)
            }
        }

        @SuppressLint("ClickableViewAccessibility")
        private fun setOnTouchListener() {
            val dragHandle = itemView.findViewById<ImageView>(R.id.drag_handle)
            dragHandle.setOnTouchListener { _, event ->
                if (event.action == MotionEvent.ACTION_DOWN) {
                    onStartDragListener?.onStartDrag(this)
                }
                false
            }
        }
    }
}
