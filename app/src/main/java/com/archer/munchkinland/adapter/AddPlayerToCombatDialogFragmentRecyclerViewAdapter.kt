package com.archer.munchkinland.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.archer.munchkinland.R
import com.archer.munchkinland.model.Player
import com.archer.munchkinland.view.fragment.CombatFragment
import java.util.*

class AddPlayerToCombatDialogFragmentRecyclerViewAdapter(private val parentFragment: CombatFragment) :
    RecyclerView.Adapter<AddPlayerToCombatDialogFragmentRecyclerViewAdapter.ViewHolder>() {
    private var players: List<Player> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.player_list_recyclerview_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(players[position])
    }

    override fun getItemCount(): Int {
        return players.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setPlayers(newDataSet: List<Player>) {
        this.players = newDataSet
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(player: Player) {
            itemView.tag = player.id.toInt()
            val sexImageView = itemView.findViewById<ImageView>(R.id.sex_imageview)
            val playerNameTextview = itemView.findViewById<TextView>(R.id.player_name_textview)
            val raceAndClassTextview = itemView.findViewById<TextView>(R.id.race_and_class_textview)
            val gearTextView = itemView.findViewById<TextView>(R.id.gear_textview)
            val levelTextView = itemView.findViewById<TextView>(R.id.level_textview)
            val combatStrengthTextView = itemView.findViewById<TextView>(R.id.combat_strength_textview)

            val raceAndClassText = "${player.race} ${player.clazz}"
            val combatStrength = player.gear + player.level

            playerNameTextview.text = player.name
            raceAndClassTextview.text = raceAndClassText
            gearTextView.text = player.gear.toString()
            levelTextView.text = player.level.toString()
            combatStrengthTextView.text = combatStrength.toString()

            if (player.sex == "Male") {
                sexImageView.setBackgroundResource(R.drawable.male_recycler_view_item_selector)
            } else {
                sexImageView.setBackgroundResource(R.drawable.female_recycler_view_item_selector)
            }

            setOnClickListener(player.id)
        }

        private fun setOnClickListener(id: Long) {
            itemView.setOnClickListener {
                parentFragment.addPlayer(id)
                parentFragment.hideAddPlayerToCombatDialog()
            }
        }
    }
}
