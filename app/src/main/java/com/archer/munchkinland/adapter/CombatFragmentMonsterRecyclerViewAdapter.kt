package com.archer.munchkinland.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.archer.munchkinland.R
import com.archer.munchkinland.model.Monster
import com.archer.munchkinland.view.fragment.CombatFragment
import java.util.*

class CombatFragmentMonsterRecyclerViewAdapter(private val parentFragment: CombatFragment) :
    RecyclerView.Adapter<CombatFragmentMonsterRecyclerViewAdapter.ViewHolder>() {
    private var monsters: List<Monster> = ArrayList()
    private lateinit var recyclerView: RecyclerView

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.monster_combat_recyclerview_item, parent, false)
        return ViewHolder(view)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(monsters[position])
    }

    override fun getItemCount(): Int {
        return monsters.size
    }

    override fun getItemId(position: Int): Long = position.toLong()

    @SuppressLint("NotifyDataSetChanged")
    fun setMonsters(newDataSet: List<Monster>) {
        this.monsters = newDataSet
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(monster: Monster) {
            val monsterNameTextView = itemView.findViewById<TextView>(R.id.monster_name_textview)
            val monsterLevelTextView = itemView.findViewById<TextView>(R.id.monster_level_value_textview)
            val monsterModifierTextView = itemView.findViewById<TextView>(R.id.monster_modifier_value_textview)
            val monsterName = "Monster $adapterPosition"
            monsterNameTextView.text = monsterName
            monsterLevelTextView.text = monster.level.toString()
            monsterModifierTextView.text = monster.modifier.toString()
            initializeTextViewOnClicks()
            initializeDeleteButtonOnClick(adapterPosition)
            initializeDeleteButtonVisibility()
        }

        private fun initializeTextViewOnClicks() {
            initializeDecreaseMonsterLevelTextViewOnClick()
            initializeIncreaseMonsterLevelTextViewOnClick()
            initializeDecreaseMonsterModifierTextViewOnClick()
            initializeIncreaseMonsterModifierTextViewOnClick()
        }

        private fun initializeIncreaseMonsterLevelTextViewOnClick() {
            val increaseMonsterLevelTextView = itemView.findViewById<TextView>(R.id.increase_monster_level_textview)
            increaseMonsterLevelTextView.setOnClickListener {
                parentFragment.increaseMonsterLevel(adapterPosition)
                parentFragment.increaseCombinedMonsterCombatStrength()
            }
        }

        private fun initializeDecreaseMonsterLevelTextViewOnClick() {
            val decreaseMonsterLevelTextView = itemView.findViewById<TextView>(R.id.decrease_monster_level_textview)
            decreaseMonsterLevelTextView.setOnClickListener {
                parentFragment.decreaseMonsterLevel(adapterPosition)
                parentFragment.decreaseCombinedMonsterCombatStrength()
            }
        }

        private fun initializeIncreaseMonsterModifierTextViewOnClick() {
            val increaseMonsterModifierTextView = itemView.findViewById<TextView>(R.id.increase_monster_modifier_textview)
            increaseMonsterModifierTextView.setOnClickListener {
                parentFragment.increaseMonsterModifier(adapterPosition)
                parentFragment.increaseCombinedMonsterCombatStrength()
            }
        }

        private fun initializeDecreaseMonsterModifierTextViewOnClick() {
            val decreaseMonsterModifierTextView = itemView.findViewById<TextView>(R.id.decrease_monster_modifier_textview)
            decreaseMonsterModifierTextView.setOnClickListener {
                parentFragment.decreaseMonsterModifier(adapterPosition)
                parentFragment.decreaseCombinedMonsterCombatStrength()
            }
        }

        private fun initializeDeleteButtonOnClick(position: Int) {
            val deleteButton = itemView.findViewById<ImageButton>(R.id.delete_button)
            deleteButton.setOnClickListener {
                parentFragment.removeMonsterFromCombat(position)
            }
        }

        private fun initializeDeleteButtonVisibility() {
            val deleteButton = itemView.findViewById<ImageButton>(R.id.delete_button)
            if (adapterPosition == 0 && itemCount == 1) {
                deleteButton.visibility = View.GONE
            } else {
                if (deleteButton.visibility == View.GONE) {
                    deleteButton.visibility = View.VISIBLE
                }
            }
        }
    }
}
