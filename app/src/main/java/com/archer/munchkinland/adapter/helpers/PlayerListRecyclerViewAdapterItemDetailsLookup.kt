package com.archer.munchkinland.adapter.helpers

import android.view.MotionEvent
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.widget.RecyclerView
import com.archer.munchkinland.adapter.PlayerListRecyclerViewAdapter

class PlayerListRecyclerViewAdapterItemDetailsLookup(private val recyclerView: RecyclerView) :
    ItemDetailsLookup<Long>() {
    override fun getItemDetails(event: MotionEvent): ItemDetails<Long>? {
        val view = recyclerView.findChildViewUnder(event.x, event.y)
        if (view != null) {
            return (recyclerView.getChildViewHolder(view) as PlayerListRecyclerViewAdapter.ViewHolder)
                .getItemDetails()
        }
        return null
    }
}
