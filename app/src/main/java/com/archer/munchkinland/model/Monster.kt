package com.archer.munchkinland.model

class Monster(
    var level: Int,
    var modifier: Int
)
