package com.archer.munchkinland.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    foreignKeys = [
        ForeignKey(
            entity = PositionRecord::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("positionId"),
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class Player(
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    var positionId: Long,
    var name: String,
    var sex: String,
    var race: String,
    var clazz: String,
    var level: Int,
    var gear: Int
)
