package com.archer.munchkinland.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PositionRecord(
    @PrimaryKey(autoGenerate = true)
    val id: Int
)
